import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;

import ASN1.Decoder;
import ASN1.Encoder;

public class ASN1Codec {
  private DSAParameter parameter;
  private DigitalSignatureAlgorithm.KeyPair keyPair;
  
  public ASN1Codec() {
    super();
    this.parameter = null;
    this.keyPair = null;
  }

  public DSAParameter getParameter() {
    return parameter;
  }

  public DigitalSignatureAlgorithm.KeyPair getKeyPair() {
    return keyPair;
  }

  public void encodeKeys(DSAParameter parameter, DigitalSignatureAlgorithm.KeyPair keyPair,
      OutputStream publicKeyOutput, OutputStream privateKeyOutput)
      throws IOException {
      Encoder encodedP = new Encoder(parameter.getP());
      Encoder encodedQ = new Encoder(parameter.getQ());
      Encoder encodedGen = new Encoder(parameter.getGen());
      
      Encoder encodedPublicKey = new Encoder(keyPair.getPublicKey());
      Encoder encodedPrivateKey = new Encoder(keyPair.getPrivateKey());
      
      
      Encoder publicKeySequence = (new Encoder()).initSequence().addToSequence(encodedP)
      .addToSequence(encodedQ).addToSequence(encodedGen).addToSequence(encodedPublicKey);
  
      Encoder privateKeySequence = (new Encoder()).initSequence().addToSequence(encodedP)
      .addToSequence(encodedQ).addToSequence(encodedGen).addToSequence(encodedPrivateKey);
              
      publicKeyOutput.write(publicKeySequence.getBytes());
      publicKeyOutput.flush();
      
      privateKeyOutput.write(privateKeySequence.getBytes());
      privateKeyOutput.flush();
  }
  
  public void decodePublicKey(InputStream inputStream) throws IOException   {
    byte temp[] = new byte[3072];
    int len = 0;
    len = inputStream.read(temp);

    byte pubKey[] = new byte[len];
    for (int i = 0; i < len; i++) {
      pubKey[i] = temp[i];
    }
    Decoder decoder = new Decoder(pubKey, 0);
    decoder = decoder.getContent();
    
    BigInteger p = decoder.getFirstObject(true).getInteger();
    BigInteger q = decoder.getFirstObject(true).getInteger();
    BigInteger gen = decoder.getFirstObject(true).getInteger();
    BigInteger publicKey = decoder.getFirstObject(true).getInteger();
    
    this.parameter = new DSAParameter(p, q, gen, q.bitLength(), p.bitLength());
    this.keyPair = new DigitalSignatureAlgorithm.KeyPair(null, publicKey);
  }
  
  public void decodePrivateKey(InputStream inputStream) throws IOException   {
    byte temp[] = new byte[3072];
    int len = 0;
    len = inputStream.read(temp);

    byte privateKeyBytes[] = new byte[len];
    for (int i = 0; i < len; i++) {
      privateKeyBytes[i] = temp[i];
    }
    Decoder decoder = new Decoder(privateKeyBytes, 0);
    decoder = decoder.getContent();
    
    BigInteger p = decoder.getFirstObject(true).getInteger();
    BigInteger q = decoder.getFirstObject(true).getInteger();
    BigInteger gen = decoder.getFirstObject(true).getInteger();
    BigInteger privateKey = decoder.getFirstObject(true).getInteger();
    
    this.parameter = new DSAParameter(p, q, gen, q.bitLength(), p.bitLength());
    this.keyPair = new DigitalSignatureAlgorithm.KeyPair(privateKey, null);
  }
  
  public void encodeSignature(DSASignature signature, OutputStream signatureOutput)
      throws IOException {
      Encoder encodedR = new Encoder(signature.getR());
      Encoder encodedS = new Encoder(signature.getS());
      
      Encoder signatureSequence = (new Encoder()).initSequence().addToSequence(encodedR)
          .addToSequence(encodedS);
              
      signatureOutput.write(signatureSequence.getBytes());
      signatureOutput.flush();
  }
  
  public DSASignature decodeSignature(InputStream inputStream) throws IOException {
    byte temp[] = new byte[3072];
    int len = inputStream.read(temp);
    
    byte sign[] = new byte[len];
    
    for (int i = 0; i < len; i++) {
        sign[i] = temp[i];
    }

    Decoder decoder = new Decoder(sign, 0);
    decoder = decoder.getContent();
    
    BigInteger r = decoder.getFirstObject(true).getInteger();
    BigInteger s = decoder.getFirstObject(true).getInteger();
    
    DSASignature signature = new DSASignature(r, s);
    return signature;
  }
}
