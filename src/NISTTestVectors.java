import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class NISTTestVectors {
  static class TestCase {
    BigInteger p;
    BigInteger q;
    BigInteger gen;
    BigInteger msg;
    BigInteger x;
    BigInteger y;
    BigInteger k;
    BigInteger r;
    BigInteger s;
    Hash.SupportedHashNunctions hashFn;

    public BigInteger getP() {
      return p;
    }

    public void setP(BigInteger p) {
      this.p = p;
    }

    public BigInteger getQ() {
      return q;
    }

    public void setQ(BigInteger q) {
      this.q = q;
    }

    public BigInteger getGen() {
      return gen;
    }

    public void setGen(BigInteger gen) {
      this.gen = gen;
    }

    public BigInteger getMsg() {
      return msg;
    }

    public void setMsg(BigInteger msg) {
      this.msg = msg;
    }

    public BigInteger getX() {
      return x;
    }

    public void setX(BigInteger x) {
      this.x = x;
    }

    public BigInteger getY() {
      return y;
    }

    public void setY(BigInteger y) {
      this.y = y;
    }

    public BigInteger getK() {
      return k;
    }

    public void setK(BigInteger k) {
      this.k = k;
    }

    public BigInteger getR() {
      return r;
    }

    public void setR(BigInteger r) {
      this.r = r;
    }

    public BigInteger getS() {
      return s;
    }

    public void setS(BigInteger s) {
      this.s = s;
    }

    public Hash.SupportedHashNunctions getHashFn() {
      return hashFn;
    }

    public void setHashFn(Hash.SupportedHashNunctions hashFn) {
      this.hashFn = hashFn;
    }
    
  }

  
  public static void main(String[] args) throws NoSuchAlgorithmException, FileNotFoundException {
    /*
     * String pStr =
     * "f5422387a66acb198173f466e987ca692fd2337af0ed1ec7aa5f2e2088d0742c2d41ded76317001ca4044115f00aff09ad59d49b07c35ec2b25088be17ac391af17575d52c232153df94f0023a0a17ca29d8548dfa08c5f034bad0bd4511ffae6b3c504c6f728d31d1e92aad9e88382a8a42b050441a747bb71dd84cb01d9ee7";
     * BigInteger p = new BigInteger(pStr, 16); String qStr =
     * "f4a9d1750b46e27c3af7587c5d019ffc99f11f25"; BigInteger q = new BigInteger(qStr, 16); String
     * genStr =
     * "7400ad91528a6c9e891f3f5fce7496ef4d01bf91a979736547049406ab4a2d2fe49fa3730cfb86a5af3ff21f5022f07e4ee0c15a88b8bd7b5f0bf8dea3863afb4f1cac16aba490d93f44be79c1cd01ce2e12dfdb75c593d64e5bf97e839526dbcc0288cd3beb2fd7941f67d138faa88f9de90901efdc752569a4d1afbd193846";
     * BigInteger gen = new BigInteger(genStr, 16);
     * 
     * DSAParameter param = new DSAParameter(p, q, gen, q.bitLength(), p.bitLength());
     * 
     * String msg =
     * "96452f7f94b9cc004931df8f8118be7e56f16a1502e00934f16c96391b83d72490be8ffa54e7f6676eb966a63ce657a6095f8d65e1cf90a0a4685daf5ae35babc6c290d13ed9152bba0cc76d2a5a401d0d1b06f63f85018f12753338a16da32461d89acef996129554b46ca9f47b612b89ad3b90c20b4547631a809b982797da";
     * BigInteger m = new BigInteger(msg, 16); byte[] msgByte = m.toByteArray();
     * 
     * String xStr = "485e8ad4a4e49a85e0397af0bb115df175ead894"; BigInteger x = new BigInteger(xStr,
     * 16); String yStr =
     * "ec86482ea1c463198d074bad01790283fb8866e53ab5e821219f0f4a25e7d0473f9cbd2ab7348625d322ea7f09ec9a15bbcc5a9ff1f3692392768970e9e865545d3aa2934148f6d0a6ec410a16d5059c58ce428912f532cbc8f9bbbcf3657367d159212c11afd856587b1b092ab1bdae3c443661e6ba27078d03eb31e63e5922";
     * BigInteger y = new BigInteger(yStr, 16);
     * 
     * DigitalSignatureAlgorithm.KeyPair keyPair = new DigitalSignatureAlgorithm.KeyPair(x, y);
     * 
     * String kStr = "dd40049049bec3ef358731c86e2fc429ff0bdd33"; BigInteger k = new BigInteger(kStr,
     * 16);
     * 
     * DigitalSignatureAlgorithm.PerMessageSecret perMsgSecret = new
     * DigitalSignatureAlgorithm.PerMessageSecret(k, k.modInverse(q));
     * 
     * String rStr = "ed4715b8d218d31b7adf0bea5165777a7414315e"; BigInteger r = new BigInteger(rStr,
     * 16); String sStr = "29c70a036aa83eb0742f1fa3f56ccead0fc0f61d"; BigInteger s = new
     * BigInteger(sStr, 16);
     * 
     * Hash hash = new Hash(256, Hash.SupportedHashNunctions.SHA_256); DSASignature signature =
     * DigitalSignatureAlgorithm.signWithPerMsgSecret(msgByte, hash, param, keyPair.getPrivateKey(),
     * perMsgSecret);
     * 
     * System.out.println(r.equals(signature.getR()));
     * System.out.println(s.equals(signature.getS()));
     * 
     * System.out.println(DigitalSignatureAlgorithm.verify(msgByte, signature, hash, param,
     * keyPair.getPublicKey()));
     */

    CommandLineArguments cmdArgs = new CommandLineArguments();
    cmdArgs.addOption(true, "-in", true); // nist test

    String usage = "Usage:\n\tjava NISTTestVectors " + " -in nist_siggen_test_vector_file\n";

    if (!cmdArgs.parseCommandLineArgs(args)) {
      System.out.println(usage);
      return;
    }

    String sigGenTestVectorFile = cmdArgs.getOptionValue("-in");

    
    List<TestCase> testCases = parseSigGenTestVector(sigGenTestVectorFile);

    for (TestCase t : testCases) {
      boolean result = signVerify(t);
      System.out.println("Msg = " + t.getMsg().toString(16));
      System.out.println("Verification_Result = " + result);
    }
  }

  private static boolean signVerify(TestCase t)
      throws NoSuchAlgorithmException {
    DSAParameter parameter = new DSAParameter(t.getP(), t.getQ(), t.getGen(), t.getQ().bitLength(), t.getP().bitLength());
    Hash hash = new Hash(t.getHashFn());

    DigitalSignatureAlgorithm.KeyPair keyPair =
        new DigitalSignatureAlgorithm.KeyPair(t.getX(), t.getY());

    DigitalSignatureAlgorithm.PerMessageSecret perMsgSecret =
        new DigitalSignatureAlgorithm.PerMessageSecret(t.getK(),
            t.getK().modInverse(parameter.getQ()));

    byte[] msgByte = t.getMsg().toByteArray();

    DSASignature signature = DigitalSignatureAlgorithm.signWithPerMsgSecret(msgByte, hash,
        parameter, keyPair.getPrivateKey(), perMsgSecret);
    
    return DigitalSignatureAlgorithm.verify(msgByte, signature, hash, parameter,
        keyPair.getPublicKey());
  }

  private static List<TestCase> parseSigGenTestVector(String sigGenTestVectorFile) throws FileNotFoundException {
    List<TestCase> testCases = new ArrayList<TestCase>();
    
    Scanner in  = new Scanner(new FileInputStream(sigGenTestVectorFile));
    Pattern patComment = Pattern.compile("^#.*");
    Pattern patNewParameters = Pattern.compile("\\[.*\\]");
    Pattern patSHA = Pattern.compile(".*SHA-([0-9]+).*");
    BigInteger p = null;
    BigInteger q = null;
    BigInteger gen = null;
    BigInteger msg = null;
    BigInteger x = null;
    BigInteger y = null;
    BigInteger k = null;
    BigInteger r = null;
    BigInteger s = null;
    Hash.SupportedHashNunctions hashFn = null;
    
    while (in.hasNextLine()) {
      String line = in.nextLine();
      if (line.trim().isEmpty()) {
        continue;
      }
      Matcher comment = patComment.matcher(line);
      if (comment.matches()) {
        continue;
      }
      Matcher newParameters = patNewParameters.matcher(line);
      if(newParameters.matches()) {
        p = null;
        q = null;
        gen = null;
        
        Matcher sha = patSHA.matcher(line);
        if (sha.matches()) {
          String shaLen = sha.group(1).trim();
          if ("1".equals(shaLen)) {
            hashFn = Hash.SupportedHashNunctions.SHA_1;
          } else if ("256".equals(shaLen)) {
            hashFn = Hash.SupportedHashNunctions.SHA_256;
          } else if ("384".equals(shaLen)) {
            hashFn = Hash.SupportedHashNunctions.SHA_384;
          } else if ("512".equals(shaLen)) {
            hashFn = Hash.SupportedHashNunctions.SHA_512;
          } else {
            hashFn = null;
          }
        } else {
          hashFn = Hash.SupportedHashNunctions.SHA_256;
        }
        continue;
      }
      
      int base = 16;
      if (line.contains("P")) {
        String[] pair = line.split("=");
        p = new BigInteger(pair[1].trim(), base);
      } else if (line.contains("Q")) {
        String[] pair = line.split("=");
        q = new BigInteger(pair[1].trim(), base);
      } else if (line.contains("G")) {
        String[] pair = line.split("=");
        gen = new BigInteger(pair[1].trim(), base);
      } else if (line.contains("Msg")) {
        String[] pair = line.split("=");
        msg = new BigInteger(pair[1].trim(), base);
      } else if (line.contains("X")) {
        String[] pair = line.split("=");
        x = new BigInteger(pair[1].trim(), base);
      } else if (line.contains("Y")) {
        String[] pair = line.split("=");
        y = new BigInteger(pair[1].trim(), base);
      } else if (line.contains("K")) {
        String[] pair = line.split("=");
        k = new BigInteger(pair[1].trim(), base);        
      } else if (line.contains("R")) {
        String[] pair = line.split("=");
        r = new BigInteger(pair[1].trim(), base);
      } else if (line.contains("S")) {
        String[] pair = line.split("=");
        s = new BigInteger(pair[1].trim(), base);
        
        if (hashFn == null) {
          continue;
        }
        
        TestCase t = new TestCase();
        t.setP(p);
        t.setQ(q);
        t.setGen(gen);
        t.setMsg(msg);
        t.setX(x);
        t.setY(y);
        t.setK(k);
        t.setR(r);
        t.setS(s);
        t.setHashFn(hashFn);
        testCases.add(t);
      }
    }
    
    return testCases;
  }

}
