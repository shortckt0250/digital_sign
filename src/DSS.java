import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

public class DSS {

  public static void main(String[] args) throws NoSuchAlgorithmException, IOException {
    CommandLineArguments cmdArgs = new CommandLineArguments();
    cmdArgs.addOption(false, "-p", true); // size_in_bits_of_p
    cmdArgs.addOption(false, "-q", true); // size_in_bits_of_q
    cmdArgs.addOption(false, "-S", true); // secret_key_file
    cmdArgs.addOption(false, "-P", true); // public_key_file
    cmdArgs.addOption(false, "-M", true); // message_file
    cmdArgs.addOption(false, "-s", true); // signature_file

    String usage = "Usage:\n\tjava DSS "
        + " [-p size_in_bits_of_p] [-q size_in_bits_of_q] [-S secret_key_file]"
        + " [-P public_key_file] [-M message_file] [-s signature_file]\n";

    if (!cmdArgs.parseCommandLineArgs(args)) {
      System.out.println(usage);
      return;
    }

    if (cmdArgs.isOptionFound("-p") && cmdArgs.isOptionFound("-q") && cmdArgs.isOptionFound("-S")
        && cmdArgs.isOptionFound("-P")) {
      // key pair generation.
      int pLen = Integer.parseInt(cmdArgs.getOptionValue("-p"));
      int qLen = Integer.parseInt(cmdArgs.getOptionValue("-q"));
      String privateKeyFile = cmdArgs.getOptionValue("-S");
      String publicKeyFile = cmdArgs.getOptionValue("-P");
      keyGen(pLen, qLen, privateKeyFile, publicKeyFile);
    } else if (cmdArgs.isOptionFound("-M") && cmdArgs.isOptionFound("-S")
        && cmdArgs.isOptionFound("-s")) {
      // signing
      String msgFile = cmdArgs.getOptionValue("-M");
      String privateKeyFile = cmdArgs.getOptionValue("-S");
      String signatureFile = cmdArgs.getOptionValue("-s");
      signMessage(msgFile, privateKeyFile, signatureFile);
    } else if (cmdArgs.isOptionFound("-M") && cmdArgs.isOptionFound("-P")
        && cmdArgs.isOptionFound("-s")) {
      // verify
      String msgFile = cmdArgs.getOptionValue("-M");
      String publicKeyFile = cmdArgs.getOptionValue("-P");
      String signatureFile = cmdArgs.getOptionValue("-s");
      verifyMessage(msgFile, publicKeyFile, signatureFile);
    } else {
      System.out.println(usage);
      return;
    }
  }

  private static void verifyMessage(String msgFile, String publicKeyFile, String signatureFile)
      throws IOException, NoSuchAlgorithmException {
    byte[] data = readMessageBytes(msgFile);

    ASN1Codec decoder = new ASN1Codec();
    decoder.decodePublicKey(new FileInputStream(publicKeyFile));
    DigitalSignatureAlgorithm.KeyPair keyPair = decoder.getKeyPair();
    DSAParameter parameter = decoder.getParameter();
    if (!DSAParameter.isValidPQLength(parameter.getP().bitLength(), parameter.getQ().bitLength())) {
      System.err.println("Invalid combination of bitlength of P and Q!");
      return;
    }
    
    ASN1Codec signuatureDecoder = new ASN1Codec();
    FileInputStream signatureInput = new FileInputStream(signatureFile);
    DSASignature signature = signuatureDecoder.decodeSignature(signatureInput);
    
    Hash hash = new Hash(Hash.SupportedHashNunctions.SHA_256);
    boolean result = DigitalSignatureAlgorithm.verify(data, signature, hash, parameter, 
        keyPair.getPublicKey());

    System.out.printf("Verification %s\n", result ? "Successful!!!" : "Failed!!!");
  }

  private static void signMessage(String msgFile, String privateKeyFile, String signatureFile)
      throws FileNotFoundException, IOException, NoSuchAlgorithmException {
    byte[] data = readMessageBytes(msgFile);


    ASN1Codec decoder = new ASN1Codec();
    decoder.decodePrivateKey(new FileInputStream(privateKeyFile));
    DigitalSignatureAlgorithm.KeyPair keyPair = decoder.getKeyPair();
    DSAParameter parameter = decoder.getParameter();
    if (!DSAParameter.isValidPQLength(parameter.getP().bitLength(), parameter.getQ().bitLength())) {
      System.err.println("Invalid combination of bitlength of P and Q!");
      return;
    }

    Hash hash = new Hash(Hash.SupportedHashNunctions.SHA_256);
    DSASignature signature =
        DigitalSignatureAlgorithm.sign(data, hash, parameter, keyPair.getPrivateKey());
    if (signature == null) {
      System.err.println("Failed to sign message.");
    }
    System.out.println("Message signature created.");
    
    ASN1Codec encoder = new ASN1Codec();
    FileOutputStream signatureOutputStream = new FileOutputStream(signatureFile);
    encoder.encodeSignature(signature, signatureOutputStream);
    signatureOutputStream.close();
  }

  public static byte[] readMessageBytes(String msgFile) throws FileNotFoundException {
    Scanner in = new Scanner(new FileInputStream(msgFile));
    StringBuilder buff = new StringBuilder();
    while (in.hasNextLine()) {
      buff.append(in.nextLine());
    }
    byte[] data = buff.toString().getBytes();
    in.close();
    return data;
  }

  private static void keyGen(int pLen, int qLen, String privateKeyFile, String publicKeyFile)
      throws NoSuchAlgorithmException, IOException {
    Hash hash = new Hash(Hash.SupportedHashNunctions.SHA_256);
    DSAParameter parameter = DSAParameter.generateDSAParameter(pLen, qLen, hash);
    if (parameter == null) {
      return;
    }
    
    DigitalSignatureAlgorithm.KeyPair keyPair =
        DigitalSignatureAlgorithm.generateKeyPair(parameter, qLen);
    FileOutputStream privateKeyOutput = new FileOutputStream(privateKeyFile);
    FileOutputStream publicKeyOutput = new FileOutputStream(publicKeyFile);
    ASN1Codec encoder = new ASN1Codec();
    encoder.encodeKeys(parameter, keyPair, publicKeyOutput, privateKeyOutput);
    privateKeyOutput.close();
    publicKeyOutput.close();
  }

}
