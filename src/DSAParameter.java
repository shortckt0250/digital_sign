import java.math.BigInteger;
import java.security.SecureRandom;

/***
 * This a factory class for generating the Digital Signature Algorithm
 * parameters.
 *
 */
public class DSAParameter {
  /***
   * The probability that a generated random number is prime is greater than 
   * 1 - (1 / 2 ^ CERTAINTY).
   */
  private static final int CERTAINTY = 30;

  /***
   * Bit Length of p.
   */
  private final int L;
  private final BigInteger p;
  /***
   * Bit Length of q.
   */
  private final int N;
  private final BigInteger q;
  /***
   * The generator.
   */
  private final BigInteger gen;
  
  private final BigInteger pSeed;    // Optional
  private final BigInteger qSeed;    // Optional
  private final Long pGenCounter;    // Optional
  private final Long qGenCounter;    // Optional
  private BigInteger domainParameterSeed;    // Optional
  private int counter;
  
  /***
   * 
   * @param p prime p.
   * @param q prime q.
   * @param gen generator.
   * @param N bit Length of q.
   * @param L bit Length of p.
   */
  public DSAParameter(BigInteger p, BigInteger q, BigInteger gen, int N, int L) {
    this(p, q, gen, N, L, null, null, null, null);
  }
  
  public DSAParameter(BigInteger p, BigInteger q, BigInteger gen, int N, int L, BigInteger pSeed, 
      BigInteger qSeed, Long pGenCounter, Long qGenCounter) {
    super();
    this.p = p;
    this.q = q;
    this.N = N;
    this.L = L;
    this.pSeed = pSeed;
    this.qSeed = qSeed;
    this.pGenCounter = pGenCounter;
    this.qGenCounter = qGenCounter;
    this.gen = gen;
  }

  public BigInteger getGen() {
    return gen;
  }

  public BigInteger getP() {
    return p;
  }

  public BigInteger getQ() {
    return q;
  }

  public int getL() {
    return L;
  }

  public int getN() {
    return N;
  }

  public BigInteger getpSeed() {
    return pSeed;
  }

  public BigInteger getqSeed() {
    return qSeed;
  }

  public long getpGenCounter() {
    return pGenCounter;
  }

  public long getqGenCounter() {
    return qGenCounter;
  }

  public BigInteger getDomainParameterSeed() {
    return domainParameterSeed;
  }

  public void setDomainParameterSeed(BigInteger domainParameterSeed) {
    this.domainParameterSeed = domainParameterSeed;
  }

  public int getCounter() {
    return counter;
  }

  public void setCounter(int counter) {
    this.counter = counter;
  }
  
  
//  /***
//   * Generates the parameters for Digital Signature Algoritm(DSA).
//   * 
//   * This version generates its own firstSeed value.
//   * 
//   * @param N length of p.
//   * @param L length of q.
//   * @return
//   */
//  public static DSAParameter generateDSAParameter(int N, int L) {
//    // SecureRandom sRNG = new SecureRandom();
//    int seedLength = N; // seedLength is required to be  >= N
//    
//    BigInteger firstSeed = getFirstSeed(N, seedLength);
//    while (firstSeed == null) {
//      firstSeed = getFirstSeed(N, seedLength);
//    }
//    
//    return generateDSAParameter(N, L, firstSeed);
//  }

  /***
   * Generates the parameters for Digital Signature Algoritm(DSA).
   * 
   * @param L bit length of p.
   * @param N bit length of q.
   * @param hash a secure hash function.
   * @return NULL on failure. On success , returns a DSAParameter object.
   */
  public static DSAParameter generateDSAParameter(int L, int N, Hash hash) {
//    SecureRandom secureRandom = new SecureRandom();
//    BigInteger p  = new BigInteger(L, CERTAINTY, secureRandom);
//    BigInteger q  = new BigInteger(N, CERTAINTY, secureRandom);
    if (!isValidPQLength(L, N)) {
      System.err.println("Invalid combination of bit length of P and Q!");
      return null;
    }
    
    int seedLen = N;
    int outLen = hash.getHashLength();
    DSAParameter primes = generatePrimesPQ(L, N, seedLen, outLen, hash);
    if (primes == null) {
      System.err.println("Primes P and Q generation Failed!");
      return null;
    }
    System.out.println("Primes P and Q successfuly generated.");
    BigInteger p = primes.getP();
    BigInteger q = primes.getQ();
    
    BigInteger gen = null;
    int count = 0;
    int maxIteration = 20;
    do {
      gen = getGenerator(p, q, L);
      count++;
    } while(!generatorValidationPartial(p, q, gen) && count < maxIteration);
    
    if (count == maxIteration) {
      System.err.println("Creating Generator g Failed!");
      return null;
    }
    System.out.println("Generator g successfuly created.");
    
    return new DSAParameter(p, q, gen, N, L);
  }
  
  
  /***
   * Appendix 1.2.1.1 
   * @param N
   * @param seedLength
   * @return Returns NULL on Failure.
   */
  protected static BigInteger getFirstSeed(int N, int seedLength) {
    // TODO: if N is not an acceptable value wrt the specification, return null.
    
    if (seedLength < N) {
      return null;
    }
    
    // Mask used to make sure firstSeed doesnt exceed seedLength bits.
    BigInteger mask = new BigInteger("1");
    mask = mask.shiftLeft(seedLength + 1);
    mask = mask.subtract(new BigInteger("1"));
    
    BigInteger firstSeed = new BigInteger("0");
    
    BigInteger minValue = new BigInteger("2");
    minValue = minValue.pow(N -1);    // firstSeed should be >= 2^(N-1)
    
    while (firstSeed.compareTo(minValue) < 0) {
      firstSeed = new BigInteger(1, generateSeed(seedLength));
      firstSeed = firstSeed.and(mask);
    }
    
    return firstSeed;
  }
  

  /***
   * Returns generator according to A.2.1. Note, the returned generator is not validated.
   * So the caller needs to use generatorValidationPartial() method to validate.
   * 
   * @param p prime number.
   * @param q prime number.
   * @param L bit length of p. 
   * @return a generator.
   */
  public static BigInteger getGenerator(BigInteger p, BigInteger q, int L) {
    SecureRandom secureRandom = new SecureRandom();
    BigInteger exp = (p.subtract(BigInteger.ONE)).divide(q);
    BigInteger gen;
    do {
      BigInteger h = new BigInteger(L, secureRandom);
      gen = h.modPow(exp, p);
    } while (gen.compareTo(BigInteger.ONE) == 0);
    
    return gen;
  }
  
  /***
   * Validates the generator generated using getGenerator functions according to A.2.2
   * 
   * @param p prime number.
   * @param q prime number.
   * @param gen generator to be validated.
   * @return True if the generator is partially valid and False otherwise.
   */
  public static boolean generatorValidationPartial(BigInteger p, BigInteger q, BigInteger gen) {
    BigInteger two = BigInteger.ONE.add(BigInteger.ONE);
    BigInteger pMinusOne = p.subtract(BigInteger.ONE);
    
    if (gen.compareTo(two) < 0) {
      return false;
    }
    if (gen.compareTo(p) >= 0) {
      return false;
    }
    if (gen.modPow(q, p).equals(BigInteger.ONE)) {
      return true;
    }
    
    return false;
  }

  /***
   * Generates primes P and Q using the algorithm specified in A.1.1.2
   * 
   * @param L The desired length of the prime p (in bits).
   * @param N The desired length of the prime q (in bits).
   * @param seedLen The desired length of the domain parameter seed; seedlen shall be equal to 
   * or greater than N.
   * @param outLen the bit length of the hash function output block and shall be equal to or
   * greater than N .
   * @return
   */
  protected static DSAParameter generatePrimesPQ(int L, int N, int seedLen, int outLen, Hash hash) {
    // check (L,N) pair
    
    if (seedLen < N || outLen < N) {
      return null;
    }
    
    int n = (int) (Math.ceil((double)L / outLen) - 1);
    int b = L - 1 - (n * outLen);
    int qTrials = 0;
    int maxQTrials = 10;
    int pTrials = 0;
    int maxPTrials = 10;
    
    BigInteger two = new BigInteger("2");
    BigInteger q = null;
    BigInteger p = null;
    
    byte[] domainParameterSeed;
    
    do {
      do {
        domainParameterSeed = generateSeed(seedLen);
        
        byte[] uBytes = hash.hash(domainParameterSeed);
        BigInteger twoPowNMinusOne = two.pow(N-1);
        BigInteger U = new BigInteger(1, uBytes).mod(twoPowNMinusOne);
        
        q = twoPowNMinusOne.add(U).add(BigInteger.ONE).subtract(U.mod(two));
      } while(q.isProbablePrime(CERTAINTY) && qTrials++ < maxQTrials);
      if (qTrials == maxQTrials) {
        return null;
      }
      
      BigInteger domainParameterSeedBI = new BigInteger(1, domainParameterSeed);
      
      BigInteger offset = BigInteger.ONE;
      for (int counter = 0; counter <= (4*L) - 1; counter++) {
        BigInteger[] v = new BigInteger[n+1];
        for (int j = 0; j <= n; j++) {
          byte[] vJ = hash.hash(domainParameterSeedBI.add(offset)
              .add(new BigInteger(Integer.toString(j)).mod(two.pow(seedLen))).toByteArray());
          v[j] = new BigInteger(1, vJ);
        }
  
        BigInteger W = v[0];
        for (int j = 1; j < n; j++) {
          W = W.add(v[j].multiply(two.pow(j * outLen)));
        }
        W = W.add(v[n].mod(two.pow(b)).multiply(two.pow(n * outLen)));
        
        BigInteger X = W.add(two.pow(L-1));
        BigInteger c = X.mod(two.multiply(q));
        p = X.subtract(c.subtract(BigInteger.ONE));
        if (p.compareTo(two.pow(L-1)) >= 0) {
          if (p.isProbablePrime(CERTAINTY)) {
            DSAParameter primePQ = new DSAParameter(p, q, null, N, L);
            primePQ.setDomainParameterSeed(domainParameterSeedBI);
            primePQ.setCounter(counter);
            return primePQ;
          }
        }
        offset = offset.add(new BigInteger(Integer.toString(n))).add(BigInteger.ONE);
      }
    } while (pTrials++ < maxPTrials);
    
    return null;
  }
  
  protected static byte[] generateSeed(int numBits) {
    SecureRandom sRNG = new SecureRandom();
    return sRNG.generateSeed(numBits % 8 == 0 ? numBits / 8 : (numBits / 8) + 1);
  }
  
  /***
   * 
   * 
   * @param L bit length of p.
   * @param N bit length of q.
   * @return
   */
  public static boolean isValidPQLength(int L, int N) {
    if (L == 1024 && N == 160) {
      return true;
    } else if (L == 2048 && N == 224) {
      return true;
    } else if (L == 2048 && N == 256) {
      return true;
    } else if (L == 3072 && N == 256) {
      return true;
    }
    return false;
  }
}
