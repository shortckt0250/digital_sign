import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/***
 * Wrapper class around secure hash functions.
 * @author meha
 *
 */
public class Hash {
  public enum SupportedHashNunctions {
    SHA_1, SHA_256, SHA_384, SHA_512
  }
  
  int hashLength;
  String hashFunctionName;
  MessageDigest messageDigest;
  
  public Hash(SupportedHashNunctions hashFn) throws NoSuchAlgorithmException {
    super();
    
    if (hashFn == SupportedHashNunctions.SHA_1) {
      this.hashLength = 160;
      hashFunctionName = "SHA-1";
    } else if (hashFn == SupportedHashNunctions.SHA_256) {
      this.hashLength = 256;
      hashFunctionName = "SHA-256";
    } else if (hashFn == SupportedHashNunctions.SHA_384) {
      this.hashLength = 384;
      hashFunctionName = "SHA-384";
    } else if (hashFn == SupportedHashNunctions.SHA_512) {
      this.hashLength = 512;
      hashFunctionName = "SHA-512";
    }
    
    messageDigest = MessageDigest.getInstance(hashFunctionName);
  }

  public int getHashLength() {
    return hashLength;
  }

  public String getHashFunctionName() {
    return hashFunctionName;
  }


  /***
   * Computes the hash value for the given byte array.
   * @param data input data.
   * @return byte array of the hash.
   */
  public byte[] hash(byte[] data) {
    messageDigest.reset();
    messageDigest.update(data);
    return messageDigest.digest();
  }
  
}
