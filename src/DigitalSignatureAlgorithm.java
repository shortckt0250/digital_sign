import java.math.BigInteger;
import java.security.SecureRandom;

public class DigitalSignatureAlgorithm {
  /***
   * The probability that a generated random number is prime is greater than 1 - (1 / 2 ^
   * CERTAINTY).
   */
  private static final int CERTAINTY = 30;

  public static class KeyPair {
    BigInteger privateKey;
    BigInteger publicKey;

    public KeyPair(BigInteger privateKey, BigInteger publicKey) {
      super();
      this.privateKey = privateKey;
      this.publicKey = publicKey;
    }

    public BigInteger getPrivateKey() {
      return privateKey;
    }

    public BigInteger getPublicKey() {
      return publicKey;
    }
  }

  public static class PerMessageSecret {
    /***
     * The per message secret.
     */
    final BigInteger k;
    /***
     * Inverse of the per message secret.
     */
    final BigInteger kInverse;

    public PerMessageSecret(BigInteger k, BigInteger kInverse) {
      super();
      this.k = k;
      this.kInverse = kInverse;
    }

    public BigInteger getK() {
      return k;
    }

    public BigInteger getkInverse() {
      return kInverse;
    }
  }

  public static DSASignature sign(byte[] data, Hash hash, DSAParameter parameter,
      BigInteger privateKey) {
    int trial = 0;
    int maxTrial = 50;
    do {
      // Randomly chose k.
      PerMessageSecret perMsgSecret = null;
      int maxKTrial = 100;
      int kTrial = 0;
      do {
        perMsgSecret = generatePerMessageSecret(parameter.getN(), parameter.getP(),
          parameter.getQ(), parameter.getGen());
      } while(perMsgSecret == null && kTrial++ < maxKTrial);
    
  
      if (perMsgSecret == null) {
        System.err.println("Unable to generate valid PerMessageSecret k and kInverse.");
        return null;
      }
      
      DSASignature signature = 
          signWithPerMsgSecret(data, hash, parameter, privateKey, perMsgSecret);
      if (signature != null) {
        return signature;
      }
      
    } while (trial++ < maxTrial);
    
    return null;
  }

  public static DSASignature signWithPerMsgSecret(byte[] data, Hash hash, DSAParameter parameter,
      BigInteger privateKey, PerMessageSecret perMsgSecret) {
    BigInteger k = perMsgSecret.getK();
    BigInteger kinverse = perMsgSecret.getkInverse();
 
    BigInteger r = (parameter.getGen().modPow(k, parameter.getP())).mod(parameter.getQ());
 
    byte[] msgDigest = hash.hash(data);
 
    BigInteger msg = new BigInteger(1, msgDigest);
    if (hash.getHashLength() > parameter.getN()) {
      // To take the left most N bits.
      int shiftBy = hash.getHashLength() - parameter.getN();
      msg.shiftRight(shiftBy);
    }
 
    BigInteger tmp = msg.add(privateKey.multiply(r));
    BigInteger s = (kinverse.multiply(tmp)).mod(parameter.getQ());
    
    try { 
      // This is to make sure s is invertible.
      BigInteger sInvese = s.modInverse(parameter.getQ());
      return new DSASignature(r, s);
    } catch (ArithmeticException e) {
      
    }
    return null;
  }

  /***
   * 
   * @param data The message data.
   * @param signature the digital signature.
   * @param hash the hash function used to hash data.
   * @param parameter DSA parameters.
   * @param publicKey public key.
   * @return True if verification success and False on failure. 
   */
  public static boolean verify(byte[] data, DSASignature signature, Hash hash,
      DSAParameter parameter, BigInteger publicKey) {
    byte[] msgDigest = hash.hash(data);
    BigInteger msg = new BigInteger(1, msgDigest);
    if (hash.getHashLength() > parameter.getN()) {
      // To take the left most N bits.
      int shiftBy = hash.getHashLength() - parameter.getN();
      msg.shiftRight(shiftBy);
    }

    BigInteger w = signature.getS().modInverse(parameter.getQ());
    BigInteger u1 = (msg.multiply(w)).mod(parameter.getQ());
    BigInteger u2 = (signature.getR().multiply(w)).mod(parameter.getQ());
    BigInteger v = parameter.getGen().modPow(u1, parameter.getP())
        .multiply(publicKey.modPow(u2, parameter.getP())).mod(parameter.getP())
        .mod(parameter.getQ());

    return v.equals(signature.getR());
  }

  /***
   * Generates private public key pairs.
   * 
   * @param parameter DSA parameters.
   * @param N bit length of prime parameter Q.
   * @return Returns private public key pairs.
   */
  public static KeyPair generateKeyPair(DSAParameter parameter, int N) {
    SecureRandom secureRandom = new SecureRandom();

    final BigInteger two = BigInteger.ONE.add(BigInteger.ONE);
    final BigInteger qMinusTwo = parameter.getQ().subtract(two);

    BigInteger privateKeyX;
    do {
      privateKeyX = new BigInteger(N, CERTAINTY, secureRandom);
    } while (privateKeyX.compareTo(qMinusTwo) > 0);

    System.out.println("Private key is created.");
    BigInteger publicKeyY = parameter.getGen().modPow(privateKeyX, parameter.getP());
    System.out.println("Public key is created.");
    return new KeyPair(privateKeyX, publicKeyY);
  }

  /***
   * Generates per mesage secret According to B.2.1
   * 
   * @param N bit length of q.
   * @param p prime number.
   * @param q prime number.
   * @param gen generator.
   * @return NULL if it failse to find a valid k, kInverse pair. Otherwiese returns k, kInverse
   *         pair.
   */
  public static PerMessageSecret generatePerMessageSecret(int N, BigInteger p, BigInteger q,
      BigInteger gen) {
    final int maxTrials = 300;
    SecureRandom secureRandom = new SecureRandom();

    int returnedBits = N + 64;
    BigInteger c = new BigInteger(returnedBits, secureRandom);
    int trials = 0;
    boolean status = true;

    BigInteger k = null;
    BigInteger kInverse = null;
    do {
      k = c.mod(q.subtract(BigInteger.ONE)).add(BigInteger.ONE);
      try {
        kInverse = k.modInverse(q);
        status = true;
      } catch (ArithmeticException e) {
        trials++;
        status = false;
      }
    } while (!status && trials < maxTrials);

    if (!status) {
      return null;
    }

    return new PerMessageSecret(k, kInverse);
  }
}
