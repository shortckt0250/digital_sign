= Files =

src/
  - ASN1Codec.java
    * A wrapper around ASN1 encoder decoder library.
  - CommandLineArguments.java
    * This clas sprovides a command line argument parser.
  - DigitalSignatureAlgorithm.java
    * Implementation of the DSA signing and verifiying methods and key pair generation.
  - DSAParameter.java
    * Implementation of DSA parameter generation. This includes generation of primes P and Q and
      the generator Gen.
  - DSASignature.java
    * Our internal repeseantation of a DSA signature. i.e. R and S values.
  - DSS.java
    * Contains the main method of the program for key pair generation, signining and verifying.
  - Hash.java
    * A wrapper around java's secure hash function implementation.
  - NISTTestVectors.java
    * the main method for conductiong tests using the NIST test vectors.
    
test/
  - DigitalSignatureAlgorithmTest.java 
    * UnitTest of DigitalSignatureAlgorithm class methods.
  - DSAParameterTest.java
    * UnitTest of DSAParameterTest class methods.

= How to Compile =
Follow these steps to compile.

  $ cd DigitalSignatureAlgorithm/src
  $ javac DSS.java 
  
Now you are ready to use the program. Have fun!! :)

= How to Run =
To run this program for generating public private key pair use the following command.

    $ java DSS -p size_in_bits_of_p -q size_in_bits_q -S secret_key_file -P public_key_file


To digitaly sign a message use the following command.

    $ java DSS -M messagefile -S secret_key_file -s signature_file
    
To verify a digital signature use the following command.

    $ java DSS -M messagefile -P public_key_file -s signature_file
    