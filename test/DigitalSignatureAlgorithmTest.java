import static org.junit.Assert.*;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DigitalSignatureAlgorithmTest {
  int L;
  int N;
  DSAParameter param;
  
  @Before
  public void setUp() throws Exception {
    L = 1024;
    N = 160;
    Hash hash = new Hash(Hash.SupportedHashNunctions.SHA_256);
    param = DSAParameter.generateDSAParameter(L, N, hash);
    assertNotNull(param);
  }

  @After
  public void tearDown() throws Exception {}

  @Test
  public void testGenerateKeyPair() {
    DigitalSignatureAlgorithm.KeyPair pair = DigitalSignatureAlgorithm.generateKeyPair(param, N);
    
    BigInteger expectedY = param.getGen().modPow(pair.getPrivateKey(), param.getP());
    assertEquals(expectedY, pair.getPublicKey());
  }

  @Test
  public void testGeneratePerMessageSecret() {
    DigitalSignatureAlgorithm.PerMessageSecret mSec = 
        DigitalSignatureAlgorithm.generatePerMessageSecret(N, param.getP(),param.getQ(),
            param.getGen());
    assertNotNull(mSec);
    assertEquals(new BigInteger("1"), mSec.getK().multiply(mSec.getkInverse()).mod(param.getQ()));
  }

  @Test
  public void testSignVerify() throws NoSuchAlgorithmException {
    DigitalSignatureAlgorithm.KeyPair kPair = DigitalSignatureAlgorithm.generateKeyPair(param, N);
    assertNotNull(kPair);
    
    Hash hash = new Hash(Hash.SupportedHashNunctions.SHA_256);
    
    String msg = "Hello Digital Signature Algorithm";
    
    DSASignature signature = DigitalSignatureAlgorithm.sign(msg.getBytes(), hash, param,
        kPair.getPrivateKey());
    
    boolean result = DigitalSignatureAlgorithm.verify(msg.getBytes(), signature, hash, param,
        kPair.getPublicKey());
    
    assertTrue("Failed to verify signature.", result);
  }

}
