import static org.junit.Assert.*;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DSAParameterTest {

  @Before
  public void setUp() throws Exception {}

  @After
  public void tearDown() throws Exception {}
  
  @Test
  public void testGenerateDSAParameter() throws NoSuchAlgorithmException {
    int L = 1024;
    int N = 160;
    Hash hash = new Hash(Hash.SupportedHashNunctions.SHA_256);
    DSAParameter param = DSAParameter.generateDSAParameter(L, N, hash);
    
    assertNotNull(param);
    assertNotNull(param.getP());
    assertNotNull(param.getQ());
    assertNotNull(param.getGen());
    
    assertEquals(L, param.getP().bitLength());
    assertEquals(N, param.getQ().bitLength());
    
    // Q sould be a prime divisor of P-1
    BigInteger pMinusOne = param.getP().subtract(BigInteger.ONE);
    assertEquals(BigInteger.ZERO, pMinusOne.mod(param.getQ()));
    
//    System.out.println(param.getP());
//    System.out.println(param.getQ());
//    System.out.println(param.getGen());
  }
  
  @Test
  public void testGeneratePQ() throws NoSuchAlgorithmException {
    int L = 1024;
    int N = 160;
    Hash hash = new Hash(Hash.SupportedHashNunctions.SHA_256);
    DSAParameter param = DSAParameter.generatePrimesPQ(L, N, N, hash.getHashLength(), hash);
    assertNotNull(param);
    assertNotNull(param.getP());
    assertNotNull(param.getQ());
    

    // Q sould be a prime divisor of P-1
    BigInteger pMinusOne = param.getP().subtract(BigInteger.ONE);
    assertEquals(BigInteger.ZERO, pMinusOne.mod(param.getQ()));
    
    assertEquals(L, param.getP().bitLength());
    assertEquals(N, param.getQ().bitLength());
    
  }

}
